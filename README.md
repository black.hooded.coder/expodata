# Expodata - Extract exhibitors and get contact data

## Instalation and running

* You will need to install `Python 3.7`: https://www.python.org/downloads/

* Once you have `Python` installed, run the following command from the command line:
```bash
. ./install.sh
```
