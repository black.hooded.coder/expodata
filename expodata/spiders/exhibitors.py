# -*- coding: utf-8 -*-
import re

import scrapy
from requests_html import HTMLSession

PHONE_NUMBER = re.compile(r'\d{3}[.-]\d{3}[.-]\d{4}')

EXHIBITOR = "//a[@class='exhibitorName']/@href"
BASE_URL = 'https://www.pcbc.com/pcbc/public'

NAME_DETAIL_PAGE = "//div[@class='panel panel-default']//div[@class='panel-body']//h1/text()"
BOOTH = "//li[contains(text(),'Booth: ')]/text()"
CITY = "//span[@class='BoothContactCity']/text()"
STATE = "//span[@class='BoothContactState']/text()"
COUNTRY = "//span[@class='BoothContactCountry']/text()"
EXHIBITOR_URL = "//a[@id='BoothContactUrl']/text()"


def cleanup(text):
    try:
        text = text.strip()
        return text
    except AttributeError:
        return ''


def format_booth(text):
    try:
        return text.replace('Booth: ', '').strip()
    except AttributeError:
        return ''


def format_city(text):
    try:
        return text.strip().strip(',')
    except AttributeError:
        return ''


class ExhibitorsSpider(scrapy.Spider):
    name = 'exhibitors'
    allowed_domains = ['pcbc.com']
    start_urls = ['https://www.pcbc.com/pcbc/public/Exhibitors.aspx']

    def parse_detail_page(self, response):
        name = cleanup(response.xpath(NAME_DETAIL_PAGE).get())
        booth = format_booth(response.xpath(BOOTH).get())
        city = format_city(response.xpath(CITY).get())
        state = cleanup(response.xpath(STATE).get())
        country = cleanup(response.xpath(COUNTRY).get())
        exhibitor_url = cleanup(response.xpath(EXHIBITOR_URL).get())

        session = HTMLSession()
        if not exhibitor_url.startswith('http'):
            urls = (f'http://{exhibitor_url}', f'https://{exhibitor_url}')
        else:
            urls = (exhibitor_url,)
        try:
            for url in urls:
                r = session.get(url)
                contact_pages = {i for i in r.html.absolute_links if 'contact' in i}
                if contact_pages:
                    break
        except:
            contact_pages = {}
            pass

        phones = set()
        email_addresses = set()
        social_media = set()
        try:
            for page in contact_pages:
                r = session.get(page)
                anchors = r.html.find('a')
                email_addresses.update(
                    [a.attrs.get('href', '').split('mailto:')[-1] for a in anchors if '@' in a.attrs.get('href', '')])
                abs_links = r.html.absolute_links
                for link in abs_links:
                    if 'facebook.com/' in link or 'twitter.com/' in link or 'linkedin' in link:
                        social_media.add(link)
                matches = PHONE_NUMBER.findall(r.text)
                if matches:
                    phones.update(matches)
        except:
            print(name)
            # raise
            pass

        return {
            'name': name,
            'booth': booth,
            'city': city,
            'state': state,
            'country': country,
            'url': exhibitor_url,
            'contact-pages': sorted(list(contact_pages)),
            'emails': sorted(list(email_addresses)),
            'phones': sorted(list(phones)),
            'social-media': sorted(list(social_media)),
        }

    def parse(self, response):
        exhibitor_links = response.xpath(EXHIBITOR).getall()

        for link in exhibitor_links:
            yield scrapy.Request(url=f'{BASE_URL}/{link}', callback=self.parse_detail_page)
