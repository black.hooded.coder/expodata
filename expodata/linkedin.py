#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv
import json
import os

from GoogleScraper import scrape_with_config, GoogleSearchError

THIS_PATH = os.path.dirname(os.path.realpath(__file__))


def get_keywords():
    with open(os.path.join(THIS_PATH, '..', 'exhibitor_details.json')) as fin:
        data = json.load(fin)
        return [f'{i["name"]} site:linkedin.com' for i in data]


def do_search():
    config = {
        'use_own_ip': True,
        'keywords': get_keywords(),
        'search_engines': ['bing'],
        'num_pages_for_keyword': 1,
        'scrape_method': 'http-async',
        'do_caching': True,
        'output_filename': 'out.json',
    }

    print(config)

    try:
        search = scrape_with_config(config)
    except GoogleSearchError as e:
        print(e)

    # let's inspect what we got. Get the last search:
    for serp in search.serps:
        print(serp)
        for link in serp.links:
            print(link)


def read_results():
    with open('out.json') as fin:
        json_data = json.load(fin)
        for search in json_data:
            name = search.get('query').split('site:')[0].strip()
            results = search.get('results')
            links = set()
            for res in results:
                title = res.get('title')
                link = res.get('link')
                if title != 'None' and link != 'None':
                    links.add((title, link))
            if links:
                yield name, links


def write_csv():
    with open('contact.csv', 'w') as fout:
        writer = csv.writer(fout)
        for name, links in read_results():
            for title, link in links:
                if title and link:
                    writer.writerow([name, title, link])


if __name__ == '__main__':
    write_csv()
