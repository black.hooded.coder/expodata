#!/usr/bin/env bash

# Install pip, venv, requirements, etc.

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

rm get-pip.py

python3 -m venv ./venv/
source ./venv/bin/activate

pip install --upgrade pip
pip install -r requirements.txt

python setup.py install

python main.py

echo "Check out the output in exhibitor_details.csv and expodata/contact.csv"
