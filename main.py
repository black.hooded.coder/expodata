# -*- coding: utf-8 -*-
import os
from inspect import currentframe, getframeinfo
from pathlib import Path

from scrapy import cmdline

from expodata.linkedin import write_csv

THIS_PATH = Path(getframeinfo(currentframe()).filename).resolve().parent

OUTPUT = os.path.join(THIS_PATH, 'exhibitor_details.json')


def scrape_detail_links():
    try:
        # os.remove(OUTPUT)
        cmdline.execute("scrapy crawl exhibitors -o exhibitor_details.csv".split())
        write_csv()
    except:
        raise
    finally:
        pass


if __name__ == '__main__':
    print('Starting crawling. This can take several minutes...')
    scrape_detail_links()
    print('Done! See output in "car_details.csv"')
